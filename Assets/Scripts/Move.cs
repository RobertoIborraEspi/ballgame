using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Move : MonoBehaviour
{
    public GameObject player;
    public float forceValue;
    public float jumpValue;
    private Rigidbody rigidBody;
    public AudioClip failSound;
    private bool jump;
    private bool fail;
    public AudioClip jumpSound;
    private float x, y, z;
    public GameObject finishLine;
    public AudioClip finishSound;
    private bool fin;
    private int puntuacion;
    public Text points;
    public GameObject camera;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        x = player.transform.position.x;
        y = player.transform.position.y;
        z = player.transform.position.z;
        jump = true;
        fail = false;
        fin = false;
        puntuacion = 0;
        points.text = puntuacion.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        //transform.Translate(Input.GetAxis("Horizontal") * speed * Time.deltaTime, 0, Input.GetAxis("Vertical") * speed * Time.deltaTime);
        if (Input.GetButtonDown("Jump") && Mathf.Abs(rigidBody.velocity.y) < 0.01f)
        {
            rigidBody.AddForce(Vector3.up * (jumpValue * 10), ForceMode.Impulse);
            AudioSource.PlayClipAtPoint(jumpSound, transform.position);
        }
        if (Input.touchCount == 1)
        {
            if (Input.touches[0].phase == TouchPhase.Began && Mathf.Abs(rigidBody.velocity.y) < 0.01f)
            {
                rigidBody.AddForce(Vector3.up * (jumpValue * 10), ForceMode.Impulse);
                AudioSource.PlayClipAtPoint(jumpSound, transform.position);
            }
        }
        if (player.transform.position.y < -5 && !fail)
        {
            AudioSource.PlayClipAtPoint(failSound, camera.transform.position);
            fail = true;
            Invoke("Restart", 2.0f);
        }
        if (finishLine == null && !fin)
        {
            fin = true;
            AudioSource.PlayClipAtPoint(finishSound, camera.transform.position);
            Invoke("NextLevel", 7.8f);
        }
    }

    void FixedUpdate()
    {
        rigidBody.AddForce(new Vector3(-(Input.GetAxis("Vertical")), 0, Input.GetAxis("Horizontal")) * (forceValue * 10));
        rigidBody.AddForce(new Vector3(-(Input.acceleration.y), 0, Input.acceleration.x) * (forceValue * 50));
    }

    public void Restart()
    {
        fail = false;
        player.transform.position = new Vector3(x, y, z);
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Coin")
        {
            puntuacion += 100;
            points.text = puntuacion.ToString();

        }


    }

    private void NextLevel()
    {
        SceneManager.LoadScene("Level2");
    }
}
