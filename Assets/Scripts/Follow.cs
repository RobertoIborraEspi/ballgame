using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Follow : MonoBehaviour
{
    public GameObject player;
    private Vector3 playerOffset;
    public Text points;
    private GUIStyle style;

    // Use this for initialization
    void Start()
    {
        playerOffset = transform.position - player.transform.position;
        style = new GUIStyle();
    }
    void OnGUI()
    {
        style.fontSize = 25;
        style.normal.textColor = Color.white;
        GUI.Label(new Rect(5, 5, 200, 20), "Accelerometer X: " + Input.acceleration.x, style);
        GUI.Label(new Rect(5, 30, 200, 20), "Accelerometer Y:" + Input.acceleration.y, style);
        GUI.Label(new Rect(5, 55, 200, 20), "Accelerometer Z:" + Input.acceleration.z, style);
        GUI.Label(new Rect(5, 80, 200, 20), "Puntuación: " + points.text, style);
    }
    // Update is called once per frame
    void Update()
    {
        //transform.LookAt(player.transform);
        transform.position = player.transform.position + playerOffset;
    }
}
