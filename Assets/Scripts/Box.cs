using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Box : MonoBehaviour
{
    public AudioClip boxSound;

    void Start()
    {

    }

    void OnCollisionEnter(Collision collider)
    {
        if (collider.gameObject.name == "Player")
        {
            AudioSource.PlayClipAtPoint(boxSound, transform.position);
        }
        
   
        
        
    }
    
   
}
