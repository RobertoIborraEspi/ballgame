using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Coin : MonoBehaviour
{
    public GameObject coin;
    public AudioClip coinSound;

    void Start()
    {

    }

    private void Update()
    {
        coin.transform.Rotate(0, 0, 1);
    }

    void OnCollisionEnter(Collision collision)
    {
          if (collision.gameObject.name == "Player")
        {
            AudioSource.PlayClipAtPoint(coinSound, transform.position);
            Destroy(coin);
        }
        
        
    }
    
   
}
